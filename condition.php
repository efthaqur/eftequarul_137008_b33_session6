<p>==================Example of While Loop=================</p>
<?php

    $terms = 5638;

    if($terms % 2 == 0) {
        echo "The number is Even!";
    }
    else {
        echo "The number is Odd!";
    }

?>
    <br />
    <p>==================Example of While Loop=================</p>
<?php

    $z = 0;

    while($z<10) {

        echo "<br />This is while  loop<br />";
        $z++;
    }


?>
<br />
<p>==================Example of Foreach Loop=================</p>
<?php
    $vowel = array("a","e","i","o","u");

    foreach($vowel as $key => $value)
        echo "\$vowel[$key]=>$value\n";
?>
<br />
<p>==================Example of Foreach Loop=================</p>
